﻿using System;

namespace Collections_LINQ.Models
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
    public struct Task
    {
        public int id;
        public int projectId;
        public User performer;
        public string name;
        public string description;
        public int state;
        public DateTime createdAt;
        public DateTime? finishedAt;

        public override string ToString()
        {
            return $"Task id: {id} {name}";
        }

    }
}
