﻿using System;
using System.Collections.Generic;

namespace Collections_LINQ.Models
{
    public struct Project
    {
        public int id;
        public User author;
        public Team team;
        public string name;
        public string description;
        public List<Task> tasks;
        public DateTime deadline;
        public DateTime createdAt;

        public override string ToString()
        {
            return $"Project id: {id} {name} ";
        }
    }

}


