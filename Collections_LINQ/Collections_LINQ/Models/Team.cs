﻿using System;

namespace Collections_LINQ.Models
{
    public struct Team
    {
        public int id;
        public string name;
        public DateTime createdAt;
    }
}
