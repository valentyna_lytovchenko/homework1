﻿using System;

namespace Collections_LINQ.Models
{
    public struct User
    {
        public int id;
        public int? teamId;
        public string firstName;
        public string lastName;
        public string email;
        public DateTime registeredAt;
        public DateTime birthDay;

        public override string ToString()
        {
            return $"User: id={id}, {firstName}, teamid={teamId}";
        }
    }
}
