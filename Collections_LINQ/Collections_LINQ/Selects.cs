﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collections_LINQ.Models;

namespace Collections_LINQ
{
    public static class Selects
    {
        static EntitiesBank bank = EntitiesBank.Instance;

        public static Dictionary<Project, int> SelectTasksNumberInProject(int userId) // select №1
        {
            Dictionary<Project, int> selected = (bank.projects.GroupJoin(
                                                    bank.tasks.Where(t => t.performer.id == userId),
                                                    proj => proj.id,
                                                    t => t.projectId,
                                                    (proj, tasks) => new KeyValuePair<Project, int>(proj, tasks.Count())
                                                 )).Where(x => x.Value > 0).
                                                 ToDictionary(x => x.Key, y => y.Value);

            return selected;
        }

        public static ICollection<Models.Task> SelectTasks(int userId) // select №2
        {
            ICollection<Models.Task> selected = (from tsk in bank.tasks
                                                 where tsk.performer.id == userId
                                                 where tsk.name.Length < 45
                                                 select tsk).ToList();

            return selected;
        }

        public static dynamic SelectTasksCompletedInCurrentYear(int userId) // select №3
        {
            var selected = (from tsk in bank.tasks
                            where tsk.performer.id == userId
                            where tsk.finishedAt.GetValueOrDefault().Year == DateTime.Now.Year
                            select new {
                                id = tsk.id,
                                name = tsk.name
                            }).ToList();

            return selected;
        }

        public static dynamic SelectTeamsWithMembers() // select №4
        {
            var selected = (bank.teams.Select(t => new { t.id, t.name}).GroupJoin(
                    bank.users,
                    t => t.id,
                    u => u.teamId,
                    (anonTeam, usersEnumerable) => new {
                        anonTeam.id,
                        anonTeam.name,
                        users = usersEnumerable.OrderByDescending(u => u.registeredAt).ToList()
                    }
                )).ToList();

            return selected;
        }

        public static dynamic SelectUsersWithTasks() // select №5
        {
            var selected = bank.users.OrderBy(u => u.firstName).GroupJoin(
                    bank.tasks.OrderByDescending(t => t.name.Length),
                    u => u.id,
                    t => t.performer.id,
                    (u, tasks) => new { user = u, tasks }
                );
            return selected;
        }

        public static dynamic SelectUserTasksInfo(int userId) // select №6
        {
            var selected = bank.users.Where(u => u.id == userId).GroupJoin(
                    bank.tasks,
                    u => u.id,
                    t => t.performer.id,
                    (u, tasks) => new { 
                        user = u,
                        countNotFinishedTasks = tasks.Count(t => (TaskState)t.state == TaskState.InProgress || (TaskState)t.state == TaskState.Canceled),
                        longestTask = tasks.OrderBy(t => t.createdAt - t.finishedAt).First()
                    }
                ).GroupJoin(
                    bank.projects,
                    prevStruct => prevStruct.user.teamId,
                    proj => proj.team.id,
                    (prevStruct, projects) => new
                        {
                            prevStruct.user,
                            prevStruct.countNotFinishedTasks,
                            prevStruct.longestTask,
                            lastProject = projects.OrderBy(p => p.createdAt).Last(),
                            countLastProjectTasks = projects.OrderBy(p => p.createdAt).Last().tasks.Count()
                    }
            ).FirstOrDefault();
            return selected;
        }

        public static dynamic GetProjectInfo() // select №7
        {
            var selected = bank.projects.Select(
                p => new
                {
                    project = p,
                    longestDescriptionTask = p.tasks.OrderByDescending(t => t.description).First(),
                    shortestNameTask = p.tasks.OrderBy(t => t.name).First(),
                    countUsers = p.description.Length > 20 || p.tasks.Count < 3 ? 
                        bank.users.Count(u => u.teamId == p.team.id) : -1
                }
            ).ToList();
            return selected;
        }
    }
}
