﻿using Collections_LINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ
{
    public class ServiceJSON
    {
        private readonly HttpClient HttpClient;

        private static ServiceJSON _instance = null;
        public static ServiceJSON Instance
        {
            get => _instance != null ? _instance : _instance = new ServiceJSON();
            private set => _instance = value;
        }

        private ServiceJSON() {
            HttpClient = new HttpClient(); 
        }

        private async Task<string> GetData(string URL)
        {
            string responseBody = "";
            try	
            {
                HttpResponseMessage response = await HttpClient.GetAsync(URL);
                response.EnsureSuccessStatusCode();
                responseBody = await response.Content.ReadAsStringAsync();
            }
            catch(HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");	
                Console.WriteLine("Message :{0} ",e.Message);
            }

            return responseBody;
        }

        public async Task<List<ProjectJSON>> GetProjects()
        {
            return JsonConvert.DeserializeObject<List<ProjectJSON>>(
                await GetData("https://bsa21.azurewebsites.net/api/Projects"));
        }

        public async Task<List<TeamJSON>> GetTeams()
        {
            return JsonConvert.DeserializeObject<List<TeamJSON>>(
                await GetData("https://bsa21.azurewebsites.net/api/Teams"));
        }

        public async Task<List<TaskJSON>> GetTasks()
        {
            return JsonConvert.DeserializeObject<List<TaskJSON>>(
                await GetData("https://bsa21.azurewebsites.net/api/Tasks"));
        }

        public async Task<List<UserJSON>> GetUsers()
        {
            return JsonConvert.DeserializeObject<List<UserJSON>>(
                await GetData("https://bsa21.azurewebsites.net/api/Users"));
        }
    }
}
