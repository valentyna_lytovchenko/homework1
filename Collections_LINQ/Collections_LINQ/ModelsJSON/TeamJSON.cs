﻿using System;

namespace Collections_LINQ.Models
{
    public struct TeamJSON
    {
        public int id;
        public string name;
        public DateTime createdAt;
    }
}
