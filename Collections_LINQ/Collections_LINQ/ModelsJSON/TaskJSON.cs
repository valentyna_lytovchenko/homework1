﻿using System;

namespace Collections_LINQ.Models
{
    public struct TaskJSON
    {
        public int id;
        public int projectId;
        public int performerId;
        public string name;
        public string description;
        public int state;
        public DateTime createdAt;
        public DateTime? finishedAt;
    }
}
