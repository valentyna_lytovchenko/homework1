﻿using System;
using System.Collections.Generic;

namespace Collections_LINQ.Models
{
    public struct ProjectJSON
    {
        public int id;
        public int authorId;
        public int teamId;
        public string name;
        public string description;
        public DateTime deadline;
        public DateTime createdAt;
    }

}


