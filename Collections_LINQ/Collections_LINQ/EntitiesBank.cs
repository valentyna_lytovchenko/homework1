﻿using Collections_LINQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections_LINQ
{
    public class EntitiesBank 
    {
        public List<Project> projects;
        public List<Models.Task> tasks = new List<Models.Task>();
        public List<Team> teams;
        public List<User> users; 

        public List<ProjectJSON> projectsJSON;
        public List<TaskJSON> tasksJSON;
        public List<TeamJSON> teamsJSON;
        public List<UserJSON> usersJSON;

        private static EntitiesBank _instance = null;
        public static EntitiesBank Instance { 
            get => _instance != null ? _instance : _instance = new EntitiesBank();
            private set => _instance = value;
        }

        private EntitiesBank() { }

        public void FillProjects(IList<ProjectJSON> projectjson) //static
        {
            projectsJSON = projectjson.ToList();
            projects = (
                from projectJ in projectsJSON
                join u in users on projectJ.authorId equals u.id
                join t in teams on projectJ.teamId equals t.id
                select new Project()
                {
                    id = projectJ.id,
                    author = u,
                    team = t,
                    tasks = tasks.Where(tj => tj.projectId == projectJ.id).ToList(),
                    name = projectJ.name,
                    description = projectJ.description,
                    deadline = projectJ.deadline,
                    createdAt = projectJ.createdAt
                }
            ).ToList();
        }

        public void FillTasks(IList<TaskJSON> taskJSONs) //static
        {
            tasksJSON = taskJSONs.ToList();
            tasks = ( 
                from taskJ in tasksJSON
                join u in users on taskJ.performerId equals u.id
                select new Models.Task()
                {
                    id = taskJ.id,
                    projectId = taskJ.projectId,
                    performer = u,
                    name = taskJ.name,
                    description = taskJ.description,
                    state = taskJ.state,
                    createdAt = taskJ.createdAt,
                    finishedAt = taskJ.finishedAt
                }
            ).ToList();
        }

        public void FillUsers(IList<UserJSON> userJSONs) //static
        {
            usersJSON = userJSONs.ToList();
            users = (
                usersJSON.Select(u => new User()
                {
                    id = u.id,
                    teamId = u.teamId,
                    firstName = u.firstName,
                    lastName = u.lastName,
                    email = u.email,
                    registeredAt = u.registeredAt,
                    birthDay = u.birthDay
                }
            ).ToList());
        }

        public void FillTeams(IList<TeamJSON> teamjson) //static
        {
            teamsJSON = teamjson.ToList();
            teams = (
                teamsJSON.Select(t => new Team()
                {
                    id = t.id,
                    name = t.name,
                    createdAt = t.createdAt
                }
            ).ToList());
        }

    }
}
