﻿using Collections_LINQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Collections_LINQ
{
    class Program
    {
        static void MenuShow()
        {
            while (true)
            {
                Console.Clear();
                int choice;

                Console.WriteLine("select 1st task");
                Console.WriteLine("select 2nd task");
                Console.WriteLine("select 3rd task");
                Console.WriteLine("select 4th task");
                Console.WriteLine("select 5th task");
                Console.WriteLine("select 6th task");
                Console.WriteLine("select 7th task");

                Console.Write("-->");
                choice = int.Parse(Console.ReadLine());

                Console.Clear();
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter user id: \n-->");
                        int task1Input = int.Parse(Console.ReadLine());
                        var tsk1 = Selects.SelectTasksNumberInProject(task1Input); // task 1
                        Console.Clear();
                        if (tsk1 == null || tsk1.Count < 0)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        foreach (var item in tsk1)
                        {
                            Console.WriteLine($"Project id: {item.Key.id}");
                            Console.WriteLine($"Tasks count: {item.Value}");
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 2:
                        Console.Write("Enter user id: \n-->");
                        int task2Input = int.Parse(Console.ReadLine());
                        var tsk2 = Selects.SelectTasks(task2Input); // task 2
                        Console.Clear();
                        if (tsk2 == null || tsk2.Count < 0)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        foreach (var item in tsk2)
                        {
                            Console.WriteLine(item);
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 3:
                        Console.Write("Enter user id: \n-->");
                        int task3Input = int.Parse(Console.ReadLine());
                        var tsk3 = Selects.SelectTasksCompletedInCurrentYear(task3Input); // task 3
                        Console.Clear();
                        if (tsk3 == null || tsk3.Count < 0)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        foreach (var item in tsk3)
                        {
                            Console.WriteLine($"Task id: {item.id}");
                            Console.WriteLine($"Task name: {item.name}");
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 4:
                        var tsk4 = Selects.SelectTeamsWithMembers(); // task 4
                        Console.Clear();
                        foreach (var item in tsk4)
                        {
                            Console.WriteLine(item.id);
                            Console.WriteLine(item.name);
                            foreach (var user in item.users)
                            {
                                Console.WriteLine(user);
                            }
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 5:
                        var tsk5 = Selects.SelectUsersWithTasks(); // task 5
                        foreach (var item in tsk5)
                        {
                            Console.WriteLine(item.user);
                            foreach (var tsk in item.tasks)
                            {
                                Console.WriteLine("\t" + tsk);
                            }
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 6:
                        Console.Write("Enter user id: \n-->");
                        int task6Input = int.Parse(Console.ReadLine());
                        var tsk6 = Selects.SelectUserTasksInfo(task6Input); // task 6
                        Console.Clear();
                        if (tsk6 == null)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        Console.WriteLine(tsk6.user);
                        Console.WriteLine("Not Finished Tasks: " + tsk6.countNotFinishedTasks);
                        Console.WriteLine("Longest Task: " + tsk6.longestTask);
                        Console.WriteLine("Count of Last-Project Tasks: " + tsk6.countLastProjectTasks);
                        break;
                    case 7:
                        var tsk7 = Selects.GetProjectInfo(); // task 7
                        foreach (var item in tsk7)
                        {
                            Console.WriteLine(item.project);
                            Console.WriteLine("Longest by description Task: " + item.longestDescriptionTask);
                            Console.WriteLine("Shortest by name Task: " + item.shortestNameTask);
                            Console.WriteLine("Count of performers: " + item.countUsers);
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    default:
                        return;
                }
                Console.ReadKey();
            }           

        }

        static async System.Threading.Tasks.Task Main(string[] args)
        {
            ServiceJSON service = ServiceJSON.Instance;
            EntitiesBank bank = EntitiesBank.Instance;

            bank.FillUsers(await service.GetUsers());
            bank.FillTeams(await service.GetTeams());
            bank.FillTasks(await service.GetTasks());
            bank.FillProjects(await service.GetProjects());

            MenuShow();
        }
    }
}
